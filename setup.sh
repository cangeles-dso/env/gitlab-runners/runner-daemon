#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 ; pwd -P)"
REGISTER_TOKEN=$(<$SCRIPT_DIR/register-token)
GITLAB_URL=$(<$SCRIPT_DIR/gitlab-url)
DEBUG_HOST=$(<$SCRIPT_DIR/debug-host)
DEBUG_PORT=$(<$SCRIPT_DIR/debug-port)
DAEMON_NAME="runner-docker"
RUNNERD="$SCRIPT_DIR/runnerd/$DAEMON_NAME"

if [ -z "$GITLAB_URL" ] || [ -z "$REGISTER_TOKEN" ];
then
  echo "'$SCRIPT_DIR/gitlab_url' or '$SCRIPT_DIR/register_token' file content empty"
  exit 1
fi

if [ ! -z $DEBUG_PORT ] && [ ! -z $DEBUG_HOST ];
then
  LISTEN_ADDR="listen_address = \"[::]:$DEBUG_PORT\""
  ADVERTISE_ADDR="advertise_address = \"$DEBUG_HOST:$DEBUG_PORT\""
  PUBLISH_PORT="-p $DEBUG_PORT:$DEBUG_PORT "
fi

set -e -x

## Create service for runner docker daemon
if [ ! -f "/lib/systemd/system/$DAEMON_NAME.service" ];
then
tee /lib/systemd/system/$DAEMON_NAME.service << EOF &> /dev/null
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-online.target docker.socket firewalld.service containerd.service
Wants=network-online.target
Requires=docker.socket containerd.service

[Service]
Type=notify
ExecStart=/usr/bin/dockerd --bridge=docker0 --data-root=$RUNNERD.data --exec-root=$RUNNERD.exec --host=unix://$RUNNERD.sock --pidfile=$RUNNERD.pid
ExecReload=/bin/kill -s HUP \$MAINPID
TimeoutSec=0
RestartSec=2
Restart=always
StartLimitBurst=3
StartLimitInterval=60s
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
Delegate=yes
KillMode=process
OOMScoreAdjust=-500

[Install]
WantedBy=multi-user.target
EOF
fi

systemctl daemon-reload
systemctl restart $DAEMON_NAME.service

## Create base config.toml 
if [ ! -f "$SCRIPT_DIR/gitlab-runner/config-toml/config.toml" ];
then
  mkdir -p $SCRIPT_DIR/gitlab-runner/config-toml
sed "/ \{2,\}$/d" << EOF | tee $SCRIPT_DIR/gitlab-runner/config-toml/config.toml &> /dev/null 
concurrent = 6
check_interval = 0

[session_server]
  session_timeout = 1800
  ${LISTEN_ADDR:=""}
  ${ADVERTISE_ADDR:=""}
EOF
fi

## Create base aws/config
if [ ! -f "$SCRIPT_DIR/gitlab-runner/aws/config" ];
then
  mkdir -p $SCRIPT_DIR/gitlab-runner/aws
tee $SCRIPT_DIR/gitlab-runner/aws/config << EOF &> /dev/null
[default]
region = 
EOF
fi

## Create base aws/credentials
if [ ! -f "$SCRIPT_DIR/gitlab-runner/aws/credentials" ];
then
  mkdir -p $SCRIPT_DIR/gitlab-runner/aws
tee $SCRIPT_DIR/gitlab-runner/aws/credentials << EOF &> /dev/null
[default]
aws_access_key_id = 
aws_secret_access_key = 
EOF
fi

## Create runner register script
if [ ! -f "$SCRIPT_DIR/gitlab-runner/register.sh" ];
then
  mkdir -p $SCRIPT_DIR/gitlab-runner
tee $SCRIPT_DIR/gitlab-runner/register.sh << EOF &> /dev/null
#!/bin/bash
docker -H unix://$RUNNERD.sock run --rm -v $SCRIPT_DIR/gitlab-runner/config-toml:/etc/gitlab-runner gitlab/gitlab-runner:v15.2.1 register \\
  --executor "docker" \\
  --description "DSO runner" \\
  --tag-list "dso-runner" \\
  --builds-dir "/tmp/builds" \\
  --docker-hostname "dso" \\
  --docker-image="docker" \\
  --docker-volumes "/tmp/builds:/tmp/builds" \\
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \\
  --docker-volumes "$SCRIPT_DIR/gitlab-runner/aws:/root/.aws" \\
  --docker-volumes "gitlab-shared-cache:/cache" \\
  --docker-memory "100m" \\
  --registration-token="$REGISTER_TOKEN" \\
  --url "$GITLAB_URL" \\
  --non-interactive
EOF
  chmod +x $SCRIPT_DIR/gitlab-runner/register.sh
fi

## Create runner start script
if [ ! -f "$SCRIPT_DIR/gitlab-runner/start.sh" ];
then
  mkdir -p $SCRIPT_DIR/gitlab-runner
tee $SCRIPT_DIR/gitlab-runner/start.sh << EOF &> /dev/null
#!/bin/bash
docker -H unix://$RUNNERD.sock run -d --rm --name dso-runner ${PUBLISH_PORT:=""}\\
  -v $SCRIPT_DIR/gitlab-runner/config-toml:/etc/gitlab-runner \\
  -v $RUNNERD.sock:/var/run/docker.sock \\
  gitlab/gitlab-runner:v15.2.1
EOF
  chmod +x $SCRIPT_DIR/gitlab-runner/start.sh
fi

chown -R $SUDO_USER:$SUDO_USER $SCRIPT_DIR/gitlab-runner

## Create docker context for runner docker daemon
if ! su -c "docker context ls" $SUDO_USER | grep $DAEMON_NAME &> /dev/null
then
  su -c "docker context create $DAEMON_NAME --default-stack-orchestrator=swarm --docker host=unix://$RUNNERD.sock" $SUDO_USER
fi

su -c "docker context use $DAEMON_NAME" $SUDO_USER