# runner-daemon
Isolate docker runner from pipeline job containers by creating a custom daemon

## Setup
If on WSL2, enable systemd support with [DamionGans](https://github.com/DamionGans/ubuntu-wsl2-systemd-script) script

1. Install [docker](https://docs.docker.com/engine/install/ubuntu/) and perfom [post-installation](https://docs.docker.com/engine/install/linux-postinstall/) steps

2. Change directory to where `setup.sh` is located and create two files for the [registration](https://docs.gitlab.com/runner/register/) process:
    ```
    touch gitlab-url register-token
    ```
    > Note: Enter the required info in the two files (e.g., `https://gitlab.com` in `gitlab-url`).

3. In the same directory, run the script as sudo:
    ```
    sudo ./setup.sh
    ```
    > Note: The `setup.sh` will create two folders: `gitlab-runner` and `runnerd`
    >
    > `gitlab-runner` folder contains the files needed to register and start the runner
    >
    > `runnerd` folder contains the files needed for the custom docker daemon

4. [Register](https://docs.gitlab.com/runner/register/#one-line-registration-command) and start the runner:

    a. To register the runner:
    ```
    ./gitlab-runner/register.sh
    ```

    b. To start the runner:
    ```
    ./gitlab-runner/start.sh
    ```

## Quick how-to WSL2 setup
Reference on [WSL2](https://docs.microsoft.com/en-us/windows/wsl/) documentation

### Enable WSL2
1. Open the windows search box (Windows logo key + S) and type "features"

2. Open "Turn Windows features on or off"

    a. Scroll down and enable "Windows Subsystem for Linux"

    b. Restart your machine

### Install Ubuntu
1. Open PowerShell and install WSL2 Ubuntu

    a. In PowerShell, enter the command to install Ubuntu
    ```
    wsl --install --distribution Ubuntu
    ```
    > Note: Wait for the installation to complete

    b. Enter a username and password when prompted

    c. Type `exit` when complete

2. Set WSL to version 2
    ```
    wsl --set-version Ubuntu 2
    ```

### Setup WSL2 resource usage
1. In Windows, open the folder `C:\Users\<windows.username>`

2. Create a new file `.wslconfig`

    a. Add the configurations to limit resource usage
    ```
    [wsl2]
    memory=4GB
    processors=2
    ```
    > Note: Set whatever limit you'd like

### WSL2 Ubuntu setup and tool installations
1. Enter WSL2 in PowerShell with the command `wsl`

    a. In WSL2 Ubuntu, change directory to your home folder
    ```
    cd ~
    ```

2. Update the `.bashrc` to start at your home directory
    ```
    echo -e "\ncd ~" >> .bashrc
    ```

3. Enable systemd support with [DamionGans](https://github.com/DamionGans/ubuntu-wsl2-systemd-script) script

4. Restart your WSL2

    a. In WSL2 Ubuntu:
    ```
    wsl.exe --shutdown
    ```
    > Note: You can execute PowerShell commands while in WSL

    b. In PowerShell:
    ```
    wsl --shutdown
    ```

5. Install [docker](https://docs.docker.com/engine/install/ubuntu/) and perfom [post-installation](https://docs.docker.com/engine/install/linux-postinstall/) steps in WSL2 Ubuntu

